FROM openjdk:11
LABEL maintainer "Grupo 2 - desafio final <grupo02.ilab@gmail.com>"

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} app.jar

EXPOSE 8089

ENTRYPOINT ["java", "-jar", "-javaagent:/newrelic/newrelic.jar", "app.jar"]
